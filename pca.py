import numpy as np
import matplotlib.pyplot as plt
import random
import pickle
import sklearn
import librosa
import glob
import os

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.spatial.distance import cosine
from sklearn.metrics import roc_curve, roc_auc_score

def array_split(mat, window, verbose=1):
    divisor, rem = np.divmod(mat.shape[0], window)
    if rem !=0:
        if verbose==2:
            print('redundant frames = {}'.format(rem))
        mat = mat[:-rem]
    mat = np.array(np.array_split(mat, divisor))
    return mat

def pca_compute(mat, nb_comp=20 , mode='test', pca=None):
    if mode=='train':
        print('---- PCA fit with components {} ----'.format(nb_comp))
        pca = PCA(n_components=nb_comp)
        pca.fit(mat)
        return pca
    return pca.transform(mat)

def get_same_choice(nb):
    l = np.arange(nb)
    return random.Random(257).choices(l, k = 50)

def enroll_list(mat):
    ind = get_same_choice(len(mat))
    enroll_mat = []
    test_mat = []
    for i in range(len(mat)):
        if i in ind:
            enroll_mat.append(mat[i])
        else:
            test_mat.append(mat[i])
    return enroll_mat, test_mat

def pca_utt(mat, pca, ss, window):
    new_mat = []
    for i in mat:
        temp = array_split(i, window)
        temp = ss.transform(temp.reshape(temp.shape[0], -1))
        temp = pca_compute(temp, mode='test' ,pca=pca)
        temp = temp.mean(0)
        new_mat.append(temp)
    return new_mat

def cosine_dist(enroll, pos, neg, pca, ss, window):
    enroll = pca_utt(enroll, pca, ss, window)
    pos = pca_utt(pos, pca, ss, window)
    neg = pca_utt(neg, pca, ss, window)
    
    cosine_pos = []
    cosine_neg = []
    for enroll_utt in enroll:
        for pos_utt in pos:
            score = 1 - cosine(enroll_utt, pos_utt)
            cosine_pos.append(score)
        for neg_utt in neg:
            score = 1 - cosine(enroll_utt, pos_utt)
            cosine_neg.append(score)
    return cosine_pos, cosine_neg    

def metrics_compute(score_pos, score_neg):
    labels = np.concatenate(([1]*len(score_pos), [0]*len(score_neg)))
    cosine_list = np.concatenate((score_pos, score_neg))
    auc = roc_auc_score(labels, cosine_list)
    print('area under curve: ', auc*100)
    far, tar, thr = roc_curve(labels, cosine_list)
    far = far*100
    frr = (1 - tar)*100
    minDiff = min([ abs(far[i] - frr[i]) for i in range(len(far)) ])
    for i in range(len(far)):
        if abs(far[i] - frr[i]) == minDiff:
            eer = (far[i]+frr[i])/2
            print('EER: ', eer)
            break
            
def main_compute(path, window_list, nb_comp_list):
    with open(path, 'rb') as output:
        data = pickle.load(output)
        
    xtrain = data['xtrain']
    xtest_neg = data['xtest_neg']
    xtest_pos = data['xtest_pos']

    for window in window_list:
        print('**** window {} *****'.format(window))
        for nb_comp in nb_comp_list:
            xtrain = data['xtrain']
            xtest_neg = data['xtest_neg']
            xtest_pos = data['xtest_pos']
            xtrain = np.concatenate(xtrain)
            print(xtrain.shape)
            xtrain = array_split(xtrain, window, verbose=2)
            print('train frames: ', xtrain.shape)
            
            xtrain = xtrain.reshape((xtrain.shape[0], -1))
            ss = StandardScaler()
            ss.fit(xtrain)
            xtrain = ss.transform(xtrain)
            pca = pca_compute(xtrain, nb_comp=nb_comp, mode='train')
            
            enroll_xtest, xtest_true = enroll_list(xtest_pos)
            score_pos, score_neg = cosine_dist(enroll_xtest, xtest_pos, xtest_neg, pca, ss, window)
            metrics_compute(score_pos, score_neg)
            
            
window_list = [21,31,41,51]
nb_comp_list = [100,50,30,20,10]
mfcc_path = 'mfcc.pickle'
log_mel_path = 'log_mel.pickle'
print('*-*-*- MFCC *-*-*-')
main_compute(mfcc_path, window_list=window_list, nb_comp_list=nb_comp_list)
print('*-*-*- LOG MEL *-*-*-')
main_compute(log_mel_path, window_list=window_list, nb_comp_list=nb_comp_list)