import numpy as np
import matplotlib.pyplot as plt
import random
import pickle
import sklearn
import librosa
import glob
import os

from sklearn.preprocessing import StandardScaler
from sklearn.svm import OneClassSVM
from scipy.spatial.distance import cosine
from sklearn.metrics import roc_curve, roc_auc_score
from sklearn.decomposition import PCA
from sklearn import metrics

def array_split(mat, window, verbose=1):
    divisor, rem = np.divmod(mat.shape[0], window)
    if rem !=0:
        if verbose==2:
            print('redundant frames = {}'.format(rem))
        mat = mat[:-rem]
    mat = np.array(np.array_split(mat, divisor))
    return mat

def svm_compute(mat, nb_comp=20, mode='test', svm=None, pca=None):
    if mode=='train':
        print('---- PCA SVM fit with components {} ----'.format(nb_comp))
        pca = PCA(n_components=nb_comp)
        pca.fit(mat)
        mat = pca.transform(mat)
        svm_clf = OneClassSVM(gamma=0.001, kernel='rbf', nu=0.01)  
        svm_clf.fit(mat)
        return svm_clf, pca
    mat = pca.transform(mat)
    return svm.decision_function(mat), svm.predict(mat)

def get_same_choice(nb):
    l = np.arange(nb)
    return random.Random(257).choices(l, k = 50)

def enroll_list(mat):
    ind = get_same_choice(len(mat))
    enroll_mat = []
    test_mat = []
    for i in range(len(mat)):
        if i in ind:
            enroll_mat.append(mat[i])
        else:
            test_mat.append(mat[i])
    return enroll_mat, test_mat

def svm_utt(mat, svm, pca, ss, window):
    new_mat = []
    bin_mat = []
    for i in mat:
        temp = array_split(i, window)
        temp = ss.transform(temp.reshape(temp.shape[0], -1))
        temp, binary = svm_compute(temp, mode='test' ,svm=svm, pca=pca)
        temp = temp.mean(0)
        binary[binary == -1] = 0
        binary = np.argmax(np.bincount(binary))
        new_mat.append(temp)
        bin_mat.append(binary)
    return new_mat, bin_mat

def compute_dist(enroll, pos, neg, svm, pca, ss, window):
    enroll, enroll_bin = svm_utt(enroll, svm, pca, ss, window)
    pos, pos_bin = svm_utt(pos, svm, pca, ss, window)
    neg, neg_bin = svm_utt(neg, svm, pca, ss, window)
    
    score_pos = np.concatenate((enroll, pos))
    score_neg = np.array(neg)
    score_pos_bin = np.concatenate((enroll_bin, pos_bin))
    score_neg_bin = np.array(neg_bin)
    
    return score_pos, score_neg, score_pos_bin, score_neg_bin 

def metrics_compute(score_pos, score_neg, score_pos_bin, score_neg_bin):
    labels = np.concatenate(([1]*len(score_pos), [0]*len(score_neg)))
    print('nb test sample: Positive {}; Negative {}'.format(len(score_pos), len(score_neg)))
    cosine_dist = np.concatenate((score_pos, score_neg))
    cosine_dist_bin = np.concatenate((score_pos_bin, score_neg_bin))
    auc = roc_auc_score(labels, cosine_dist)
    print("accuracy: ", metrics.accuracy_score(labels, cosine_dist_bin)*100)  
    print("precision: ", metrics.precision_score(labels, cosine_dist_bin)*100)  
    print("recall: ", metrics.recall_score(labels, cosine_dist_bin)*100)  
    print("f1: ", metrics.f1_score(labels, cosine_dist_bin)*100)  
    print('area under curve: ', auc*100)
    far, tar, thr = roc_curve(labels, cosine_dist)
    far = far*100
    frr = (1 - tar)*100
    minDiff = min([ abs(far[i] - frr[i]) for i in range(len(far)) ])
    for i in range(len(far)):
        if abs(far[i] - frr[i]) == minDiff:
            eer = (far[i]+frr[i])/2
            print('EER: ', eer)
            break
            
def main_compute(path, window_list, nb_comp_list):
    with open(path, 'rb') as output:
        data = pickle.load(output)
        
    xtrain = data['xtrain']
    xtest_neg = data['xtest_neg']
    xtest_pos = data['xtest_pos']

    for window in window_list:
        print('**** window {} *****'.format(window))
        for nb_comp in nb_comp_list:
            xtrain = data['xtrain']
            xtest_neg = data['xtest_neg']
            xtest_pos = data['xtest_pos']
            xtrain = np.concatenate(xtrain)
            print(xtrain.shape)
            xtrain = array_split(xtrain, window, verbose=2)
            print('train frames: ', xtrain.shape)
            
            xtrain = xtrain.reshape((xtrain.shape[0], -1))
            ss = StandardScaler()
            ss.fit(xtrain)
            xtrain = ss.transform(xtrain)
            svm, pca = svm_compute(xtrain, nb_comp=nb_comp ,mode='train')
            
            enroll_xtest, xtest_true = enroll_list(xtest_pos)
            score_pos, score_neg, score_pos_bin, score_neg_bin = compute_dist(enroll_xtest, xtest_pos, xtest_neg, svm, pca, ss, window)
            metrics_compute(score_pos, score_neg, score_pos_bin, score_neg_bin)
            
            
window_list = [21,31,41,51]
nb_comp_list = [100,50,30,20,10]
mfcc_path = 'mfcc.pickle'
log_mel_path = 'log_mel.pickle'
print('*-*-*- MFCC *-*-*-')
main_compute(mfcc_path, window_list=window_list, nb_comp_list=nb_comp_list)
print('*-*-*- LOG MEL *-*-*-')
main_compute(log_mel_path, window_list=window_list, nb_comp_list=nb_comp_list)