import numpy as np
import matplotlib.pyplot as plt
import pickle
import sklearn
import librosa
import glob
import os


spk_id = 'id03347' #spk_ID
path = '../aac' #path to voxCeleb2 database

# get training data
nb_seconds = 70
train_paths = []
true_test_paths = []
len_temp = 0
switch = True
for i in glob.glob(path +'/'+ spk_id + '/*/*'):
    if switch:
        w, sr = librosa.load(i, mono=True, sr=16000)
        len_temp += len(w)/sr
        train_paths.append(i)
        if len_temp > nb_seconds:
            print('Total Length of training samples is {} seconds'.format(int(len_temp)) )
            switch = False
    else:
        true_test_paths.append(i)
        
# get negative test paths
all_negative_test_path = []
for i in glob.glob(path + '/*/*/*'):
    if spk_id in i.split('/'):
        continue
    all_negative_test_path.append(i)
    
# get random test paths
false_test_paths = np.random.choice(all_negative_test_path, 2000)


def log_spec_floor(mat):
    mat = 20*np.log10(mat + 1e-10)
    logfloor = -50
    mat[mat < logfloor] = -50
    return mat

#mel_spec
def log_mel_spec_compute(paths):
    all_mfcc = []
    for i in paths:
        w, sr = librosa.load(i, mono=True, sr=16000)
        x = librosa.feature.melspectrogram(y=w, sr=16000, S=None,  n_fft=512, hop_length=128, n_mels=32)
        x = log_spec_floor(x)
        all_mfcc.append(x.T)    
    return all_mfcc

#mfcc
def mfcc_compute(paths):
    all_mfcc = []
    for i in paths:
        w, sr = librosa.load(i, mono=True, sr=16000)
        x = librosa.feature.mfcc(y=w, sr=16000, S=None,  n_mfcc=32, n_fft=512, hop_length=128)
        all_mfcc.append(x.T)    
    return all_mfcc


log_mel_xtrain = log_mel_spec_compute(train_paths)
log_mel_xtest_true = log_mel_spec_compute(true_test_paths)
log_mel_xtest_false = log_mel_spec_compute(false_test_paths)

log_mel_data = { 'xtrain': log_mel_xtrain,
                'xtest_pos': log_mel_xtest_true,
                'xtest_neg': log_mel_xtest_false
                }

with open("log_mel.pickle", "wb") as output_file_mel:
    pickle.dump(log_mel_data, output_file_mel)
    
mfcc_xtrain = mfcc_compute(train_paths)
mfcc_xtest_true = mfcc_compute(true_test_paths)
mfcc_xtest_false = mfcc_compute(false_test_paths)


mfcc_data = { 'xtrain': mfcc_xtrain,
              'xtest_pos': mfcc_xtest_true,
              'xtest_neg': mfcc_xtest_false
             }

with open("mfcc.pickle", "wb") as output_file_mfcc:
    pickle.dump(mfcc_data, output_file_mfcc)



