import numpy as np
import matplotlib.pyplot as plt
import random
import pickle
import sklearn
import librosa
import glob
import os

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from scipy.spatial.distance import cosine
from sklearn.metrics import roc_curve, roc_auc_score
import pandas as pd
from sklearn import utils
import matplotlib
from scipy.optimize import minimize


nu = 0.04
K  = 100


def relu(x):
    y = x
    y[y < 0] = 0
    return y

def dRelu(x):
    y = x
    y[x <= 0] = 0
    y[x > 0]  = np.ones((len(x[x > 0]),))
    return y

def nnScore(X, w, V, g):
    return g(X.dot(V)).dot(w)

def ocnn_obj(theta, X, nu, D, K, g, dG):
    
    w = theta[:K]
    V = theta[K:K+K*D].reshape((D, K))
    r = theta[K+K*D:]
    
    term1 = 0.5  * np.sum(w**2)
    term2 = 0.5  * np.sum(V**2)
    term3 = 1/nu * np.mean(relu(r - nnScore(X, w, V, g)))
    term4 = -r
    
    return term1 + term2 + term3 + term4

def ocnn_grad(theta, X, nu, D, K, g, dG):
    
    N = X.shape[0]
    w = theta[:K]
    V = theta[K:K+K*D].reshape((D, K))
    r = theta[K+K*D:]
    
    deriv = dRelu(r - nnScore(X, w, V, g))    

    term1 = np.concatenate(( w,
                             np.zeros((V.size,)),
                             np.zeros((1,)) ))

    term2 = np.concatenate(( np.zeros((w.size,)),
                             V.flatten(),
                             np.zeros((1,)) ))

    term3 = np.concatenate(( 1/nu * np.mean(deriv[:,np.newaxis] * (-g(X.dot(V))), axis = 0),
                             1/nu * np.mean((deriv[:,np.newaxis] * (dG(X.dot(V)) * -w)).reshape((N, 1, K)) * X.reshape((N, D, 1)), axis = 0).flatten(),
                             1/nu * np.array([ np.mean(deriv) ]) ))
    
    term4 = np.concatenate(( np.zeros((w.size,)),
                             np.zeros((V.size,)),
                             -1 * np.ones((1,)) ))
    
    return term1 + term2 + term3 + term4



def One_Class_NN_explicit_linear(data_train,data_test):


    X  = data_train
    D  = X.shape[1]

    g  = lambda x : x
    dG = lambda x : np.ones(x.shape)

    np.random.seed(42)
    theta0 = np.random.normal(0, 1, K + K*D + 1)

    from scipy.optimize import check_grad
    print('Gradient error: %s' % check_grad(ocnn_obj, ocnn_grad, theta0, X, nu, D, K, g, dG))

    res = minimize(ocnn_obj, theta0, method = 'L-BFGS-B', jac = ocnn_grad, args = (X, nu, D, K, g, dG),
                   options = {'gtol': 1e-8, 'disp': True, 'maxiter' : 50000, 'maxfun' : 10000})

    thetaStar = res.x

    wStar = thetaStar[:K]
    VStar = thetaStar[K:K+K*D].reshape((D, K))
    rStar = thetaStar[K+K*D:]

    pos_decisionScore = nnScore(data_train, wStar, VStar, g) - rStar
    neg_decisionScore = nnScore(data_test, wStar, VStar, g) - rStar

    print("pos_decisionScore", np.sort(pos_decisionScore))
    print("neg_decisionScore", np.sort(neg_decisionScore))


    return [pos_decisionScore,neg_decisionScore]


def One_Class_NN_explicit_sigmoid(data_train,data_test_pos, data_test_neg):

    X  = data_train
    D  = X.shape[1]

    g   = lambda x : 1/(1 + np.exp(-x))
    dG  = lambda x : 1/(1 + np.exp(-x)) * 1/(1 + np.exp(+x))

    np.random.seed(42)
    theta0 = np.random.normal(0, 1, K + K*D + 1)

    from scipy.optimize import check_grad
    print('Gradient error: %s' % check_grad(ocnn_obj, ocnn_grad, theta0, X, nu, D, K, g, dG))

    res = minimize(ocnn_obj, theta0, method = 'L-BFGS-B', jac = ocnn_grad, args = (X, nu, D, K, g, dG),
                   options = {'gtol': 1e-8, 'disp': True, 'maxiter' : 500000, 'maxfun' : 100000})

    thetaStar = res.x

    wStar = thetaStar[:K]
    VStar = thetaStar[K:K+K*D].reshape((D, K))
    rStar = thetaStar[K+K*D:]
    
    pos_decisionScore = [ np.mean(nnScore(i, wStar, VStar, g) - rStar) for i in data_test_pos ]
    neg_decisionScore = [ np.mean(nnScore(i, wStar, VStar, g) - rStar) for i in data_test_neg ]

    return pos_decisionScore,neg_decisionScore

def array_split(mat, window, verbose=1):
    divisor, rem = np.divmod(mat.shape[0], window)
    if rem !=0:
        if verbose==2:
            print('redundant frames = {}'.format(rem))
        mat = mat[:-rem]
    mat = np.array(np.array_split(mat, divisor))
    return mat

def metrics_compute(score_pos, score_neg):
    labels = np.concatenate(([1]*len(score_pos), [0]*len(score_neg)))
    cosine_dist = np.concatenate((score_pos, score_neg))
    auc = roc_auc_score(labels, cosine_dist)
    print('area under curve: ', auc*100)
    far, tar, thr = roc_curve(labels, cosine_dist)
    far = far*100
    frr = (1 - tar)*100
    minDiff = min([ abs(far[i] - frr[i]) for i in range(len(far)) ])
    for i in range(len(far)):
        if abs(far[i] - frr[i]) == minDiff:
            eer = (far[i]+frr[i])/2
            print('EER: ', eer)
            break
            
def main_compute(path, window_list):
    with open(path, 'rb') as output:
        data = pickle.load(output)

    for window in window_list:
        print('**** window {} *****'.format(window))
        xtrain = data['xtrain']
        xtest_neg = data['xtest_neg']
        xtest_pos = data['xtest_pos']
        xtrain = np.concatenate(xtrain)
        print(xtrain.shape)
        xtrain = array_split(xtrain, window, verbose=2)
        print('train frames: ', xtrain.shape)
            
        xtrain = xtrain.reshape((xtrain.shape[0], -1))
        ss = StandardScaler()
        ss.fit(xtrain)
        xtrain = ss.transform(xtrain)
            
        xtest_pos = [ array_split(i, window) for i in xtest_pos  ]
        xtest_pos = [ ss.transform(i.reshape((i.shape[0], -1))) for i in xtest_pos ]
        xtest_neg = [ array_split(i, window) for i in xtest_neg ]
        xtest_neg = [ ss.transform(i.reshape((i.shape[0], -1))) for i in xtest_neg ]
        score_pos, score_neg = One_Class_NN_explicit_sigmoid(xtrain, xtest_pos, xtest_neg)
        metrics_compute(score_pos, score_neg)
        
window_list = [21, 31, 41, 51]
mfcc_path = 'mfcc.pickle'
log_mel_path = 'log_mel.pickle'
print('*-*-*- MFCC *-*-*-')
main_compute(mfcc_path, window_list=window_list)
print('*-*-*- LOG MEL *-*-*-')
main_compute(log_mel_path, window_list=window_list)
            
